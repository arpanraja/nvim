local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
    packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    download_result = fn.system({'ls', '-l', install_path})

    vim.cmd [[packadd packer.nvim]] -- packadd packer module

end


vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

local status_ok, packer = pcall(require,"packer")
if not status_ok then
    return
end

packer.init{
    display = {
	open_fn = function()
		return require("packer.util").float { border = "rounded" }

	end
    },

}



return packer.startup(function(use)

    use "wbthomason/packer.nvim"
    use 'ellisonleao/gruvbox.nvim'
    use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
    use "nvim-lua/plenary.nvim" -- Useful lua functions used ny lots of plugins
    -- cmp plugins
    use "hrsh7th/nvim-cmp" -- The completion plugin
    use "hrsh7th/cmp-buffer" -- buffer completions
    use "hrsh7th/cmp-path" -- path completions
    use "hrsh7th/cmp-cmdline" -- cmdline completions
    use "saadparwaiz1/cmp_luasnip" -- snippet completions
    use "hrsh7th/cmp-nvim-lsp"
    use "hrsh7th/cmp-nvim-lua"

    -- snippets
    use "L3MON4D3/LuaSnip" --snippet engine
    use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

    -- LSP
    use "neovim/nvim-lspconfig" -- enable LSP
    use "williamboman/nvim-lsp-installer" -- simple to use language server installer


    -- Telescope
    use "nvim-telescope/telescope.nvim"
    use "nvim-telescope/telescope-fzy-native.nvim"
    -- Toggle-term
    use "akinsho/toggleterm.nvim"

    --treesitter
    use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    }
    use "p00f/nvim-ts-rainbow"
    --nvim_tree
    use 'kyazdani42/nvim-web-devicons'
    use 'kyazdani42/nvim-tree.lua'

    --buffers
    use "akinsho/bufferline.nvim"
    use "moll/vim-bbye"

    --alpha
    use 'goolord/alpha-nvim'

    --multiterm
--    use 'nikvdp/neomux'

  if packer_bootstrap then
    require('packer').sync(use)
  end

end)

